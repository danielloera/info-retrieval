import React, { Component } from 'react';
import './App.css';
import Async from 'react-promise'
import Container from 'react-bootstrap/Container';
import Color from 'color';
import Card from 'react-bootstrap/Card';
import getAverageColor from 'get-average-color'
import bestContrast from 'get-best-contrast-color';
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import ReactTextCollapse from 'react-text-collapse';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Pagination from './Pagination';
import Select from 'react-select';

const TMDB_IMG_PREFIX = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
const DATE_OPTIONS = { year: 'numeric', month: 'long', day: 'numeric' };
const FONT_COLORS = ["white", "black"];
const drop = [
  { value: '', label: 'None'},
  { value: 'title', label: 'Title' },
  { value: 'directors', label: 'Director' },
  { value: 'genres', label: 'Genre' },
  { value: 'cast', label: 'Cast' },
  { value: 'characters', label: 'Character'}
];

const ID_TO_LABEL = {
  title: "Title",
  directors: "Director",
  genres: "Genre",
  cast: "Cast",
  characters: "Characters"
};

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {currentStyle: {cursor: "auto"}};
  }

  render() {
    let cursor = "pointer";
    let onClick = this.props.onClickFn;
    if (!onClick) {
      onClick = (text) => {};
      cursor = "auto";
    }
    const normalStyle = { cursor: cursor };
    const hoverStyle = { cursor: cursor, textDecoration: "underline"};
    return (<div
              onClick={()=>{onClick({id: this.props.id, value: this.props.value})}}
              style={this.state.currentStyle}
              onMouseEnter={()=> this.setState({currentStyle: hoverStyle})}
              onMouseLeave={()=> this.setState({currentStyle: normalStyle})}>
                {this.props.value}
            </div>);
  }
}

function createCollapseText(items, onClick, id) {
  const itemElements = items.map((item) => <Item value={item} onClickFn={onClick} id={id}/>);
  if (items.length < 5) {
    return itemElements;
  }
  return (
      <ReactTextCollapse
        options={{
          collapse: false, // default state when component rendered
          collapseText: <b style={{cursor: "pointer"}}>show more...</b>, // text to show when collapsed
          expandText: <b style={{cursor: "pointer"}}>show less</b>, // text to show when expanded
          minHeight: 100, // component height when closed
          maxHeight: 200 }}>
       {itemElements}
      </ReactTextCollapse>
    );
}


function createFilmCardFn(onClick) {
  return (doc) => {
    const imgSrc = TMDB_IMG_PREFIX + doc.poster_path[0];
    const date = new Date(Date.parse(doc.release_date[0]));
    const dateString = date.toLocaleDateString("en-US", DATE_OPTIONS);
    const cast = createCollapseText(doc.cast, onClick, "cast");
    const characters = createCollapseText(doc.characters, onClick, "characters");
    const genres = createCollapseText(doc.genres, onClick, "genres");
    let directors = "N/A";
    if (doc.directors) {
      directors = createCollapseText(doc.directors, onClick, "directors");
    }
    let overview = "N/A";
    if (doc.overview) {
      overview = doc.overview[0];
    }
    return (
      <Async
        promise={getAverageColor(imgSrc)}
        then={(rgb) => {
          const color = `rgb(${rgb.r},${rgb.g},${rgb.b})`;
          const lighterColor = Color(color).lighten(0.3);
          const fontColor = bestContrast(color, FONT_COLORS);
          const lightBg = {backgroundColor: lighterColor};
          return (<Card
                    style={{marginBottom: "3%", backgroundColor: color}}
                    text={fontColor}>
                    <Card.Header as="h5">{doc.title[0]}</Card.Header>
                    <Card.Img variant="top" src={imgSrc} />
                    <Card.Body>
                      <Card.Text>
                        <b>Summary</b><br></br>{overview}
                      </Card.Text>
                      <ListGroup className="list-group-flush">
                        <ListGroupItem style={lightBg}><b>Genres</b>{genres}</ListGroupItem>
                        <ListGroupItem style={lightBg}><b>Directors</b>{directors}</ListGroupItem>
                        <ListGroupItem style={lightBg}><b>Cast</b>{cast}</ListGroupItem>
                        <ListGroupItem style={lightBg}><b>Characters</b>{characters}</ListGroupItem>
                        <ListGroupItem style={lightBg}><b>Released:</b>{" " + dateString}</ListGroupItem>
                      </ListGroup>
                    </Card.Body>
                  </Card>);
        }}
      />
    );
  }
}

class App extends Component {

  constructor(props) {
    super(props);
    this.searchBox = null;
    this.state = {query: "",field: "", fieldLabel: "None"};
  }

  updateQuery() {
    var queryStr = this.searchBox.value;
    if(this.state.field !== "" && this.state.field !== undefined) {
      queryStr = `${this.state.field}:"${queryStr}"`;
    }
    this.setState({query: queryStr});
  }

  itemClicked(item) {
    const queryStr = `${item.id}:"${item.value}"`;
    this.searchBox.value = item.value;
    this.setState({
      query: queryStr,
      field: item.id,
      fieldLabel: ID_TO_LABEL[item.id]
    });
  }

  fieldClicked(selectedOption) {
    this.setState({field: selectedOption.value,
      fieldLabel: selectedOption.label});
  }

  render() {
    return (
      <div className="App">
        <Container>
          <Row style={{marginTop: "15%"}}>
            <Col>
            <Form.Label><b><font size="12">TBMDB</font></b></Form.Label>
            </Col>
          </Row>
          <Row>
            <Col xs={2}>
              <Select
                  type = "string"
                  value={this.state.field}
                  onChange={this.fieldClicked.bind(this)}
                  options={drop}
                  placeholder={this.state.fieldLabel}
              />
            </Col>
            <Col>
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  this.updateQuery();
                }}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Control
                    type="string"
                    placeholder="Search Movies"
                    ref={(input) => {this.searchBox = input}}
                    />
                  <Form.Text className="text-muted">
                  </Form.Text>
                </Form.Group>
              </Form>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col>
              <Button
                style={{marginBottom: "5%"}}
                variant="primary"
                type="submit"
                onClick={this.updateQuery.bind(this)}>
                    GO
              </Button>
            </Col>
          </Row>
          <Pagination
            rowCount={4}
            colCount={3}
            query={this.state.query}
            cardFn={createFilmCardFn(this.itemClicked.bind(this))}
          />
        </Container>
      </div>
    );
  }
}

export default App;
