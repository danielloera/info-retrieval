import React, { Component } from 'react';
import axios from 'axios';
import { PulseLoader } from 'react-spinners';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import CardColumns from 'react-bootstrap/CardColumns';

const DEF_TYPE = "edismax";
const TITLE_BOOST = 1.1;
const OVERVIEW_BOOST = 1;
const GENRES_BOOST = 1;
const DIRECTORS_BOOST = 1;
const CAST_BOOST = 1;
const CHARACTERS_BOOST = 1;

const LOAD_MSG = <PulseLoader
                    sizeUnit={"vh"}
                    size={1}
                    color={'#FFFFFF'}
                    loading={true}
                 />;
const MORE_MSG = "Load More";
const FINISHED_MSG = "You've reached the end!";
const EMPTY_MSG = <div>No results found{" "}
                    <span
                      role="img"
                      aria-label="crying face">
                        😢
                    </span>
                  </div>;

const CENTER_DIV = {
  display:"flex",
  justifyContent:"center",
  alignItems:"flex-end"
};

function getInitialState() {
  return {
    activePages: 0,
    loading: false,
    buttonMsg: LOAD_MSG,
    items: [],
    rows: []
  };
}

class Pagination extends Component {

  constructor(props) {
    super(props);
    this.pageSize = props.rowCount * this.props.colCount;
    this.state = getInitialState();
  }

  componentDidMount() {
    this.loadPage();
  }

  componentDidUpdate(prevProps, prevState) {
    const pageUpdated = this.state.activePages !== prevState.activePages;
    const endpointUpdated = this.props.query !== prevProps.query;
    if (pageUpdated && !endpointUpdated) {
      this.updateRows();
    } else if (endpointUpdated) {
      this.setState(getInitialState(), this.loadPage);
    }
  }

  loadPage() {
    let query = this.props.query;
    if (query === "" || query === ':""' || query === ':"*"') {
      query = "*:*";
    }
    const offset = this.state.activePages * this.pageSize;
    const qf = `overview^${OVERVIEW_BOOST} title^${TITLE_BOOST} genres^${GENRES_BOOST} directors^${DIRECTORS_BOOST} cast^${CAST_BOOST} characters^${CHARACTERS_BOOST}`;
    console.log("QUERY PARAMS:");
    console.log("defType: " + DEF_TYPE);
    console.log("q: " + query);
    console.log("qf: " + qf);
    console.log("rows: " + this.pageSize);
    console.log("start: " + offset + "\n");
    const url = `http://34.218.248.128:8983/solr/films/select?defType=${DEF_TYPE}&qf=${qf}&q=${query}&rows=${this.pageSize}&start=${offset}`;
    if (!this.state.loading) {
      this.setState(
        { loading: true,
          buttonMsg: LOAD_MSG },
        () => { axios.get(url)
                  .then(
                    (response) =>
                      {
                        this.setState(
                          { items: [...this.state.items , ...response.data.response.docs],
                            activePages: this.state.activePages + 1,
                            loading: false,
                            buttonMsg: MORE_MSG },
                          this.updateRows);
                      });
              });
    }
  }

  updateRows() {
    if (this.state.items.length < 1) {
      this.setState({ buttonMsg: EMPTY_MSG });
      return;
    }
    const { rowCount, colCount } = this.props;
    const { items, activePages, rows } = this.state;
    // Zero-based active page
    const start = (activePages - 1) * this.pageSize;
    const end = start + this.pageSize;
    const pageItems = items.slice(start, end);
    // Check if there are any more items to display.
    if (pageItems.length < 1) {
      this.setState({ buttonMsg: FINISHED_MSG });
      return;
    }
    // Apply function to responses to create page cards.
    const pageComponents = pageItems.map(this.props.cardFn);
    // Fill pageComponents to at least 1 page.
    while (pageComponents.length < this.pageSize) {
      pageComponents.push(null);
    }
    // Generate rows for the new page.
    let sliceIdx = 0;
    const newRows = [];
    for (let r = 0; r < rowCount; r++) {
      const columns = pageComponents
        .slice(sliceIdx, sliceIdx + colCount)
        .filter((col) => !!col);
      newRows.push(<Row key={r}>
                      <CardColumns>
                        {columns}
                      </CardColumns>
                   </Row>);
      sliceIdx += colCount;
    }
    this.setState({
      'rows': [...rows, ...newRows]
    });
  }

  render() {
    return (
    <div className="container">
      {this.state.rows}
      <div style={CENTER_DIV}>
        <Button
          onClick={this.loadPage.bind(this)}>
            {this.state.buttonMsg}
        </Button>
      </div>
      <br/>
    </div>
    );
  }
}

export default Pagination;
