import re

search_pairs = dict()

with open('gold_standard.txt') as gs:
    contents = gs.readlines()

    for line in contents:
        terms = re.split('\[*(", )*"\]*', line)
        search_pairs.setdefault(terms[6], []).append(terms[2])

import pysolr

solr = pysolr.Solr('http://34.218.248.128:8983/solr/films')

correct = 0
total = 0

for title in search_pairs:
    for query in search_pairs[title]:
        total += 1
        results = [result['title'][0] for result in solr.search(query.replace('\\', r'\\'))]
        if title in results:
            correct += 1

print('Out of {} queries, {} successfully returned the target movie in the top 10 results.'.format(total,
                                                                                                   correct))
print('Accuracy: {:0.2f}%'.format(correct / total * 100))
