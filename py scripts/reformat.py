import json
import pysolr


# Setup a Solr instance. The timeout is optional.
solr = pysolr.Solr('http://admin:pnlp2019@34.218.248.128:8983/solr/films/', timeout=10)

with open('tmdb_clean.json') as f:
	data = json.load(f)


solr.add(data)

# with open('new_data.json') as out:
# 	json.load(out)